//* new views
import RunView from '../views/survey/RunView'

const SurveyRoutes = [
  {
    path: `/survey`,
    sidebarName: 'Опрос',
    navbarName: 'Run',
    component: RunView
  }
  // {
  //   redirect: true,
  //   path: `/`,
  //   to: `/admin/dashboard`,
  //   navbarName: 'Redirect'
  // }
]

export default SurveyRoutes
