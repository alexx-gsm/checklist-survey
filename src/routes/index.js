import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
// Layouts
import SurveyLayout from '../layouts/Survey'

class Routes extends React.Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path='/survey' component={SurveyLayout} />
          <Route path='/' render={() => <h1>Опрос</h1>} />
        </Switch>
      </Router>
    )
  }
}

export default Routes
