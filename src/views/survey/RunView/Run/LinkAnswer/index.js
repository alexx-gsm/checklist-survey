import { compose, withState, withProps, withHandlers } from 'recompose'
import { connect } from 'react-redux'
// AC
import { setSummary } from '../../../../../redux/modules/result'
// Hocs
import withDialog from '../../../../../hocs/withDialog'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import CommonStyles from '../../../../../styles/CommonStyles'
import CardStyles from '../../../../../styles/CardStyles'
import CardTableStyles from '../../../../../styles/CardTableStyles'

import LinkAnswer from './LinkAnswer'

export default compose(
  connect(
    ({ resultStore }) => {
      const { data, question, summary } = resultStore

      return {
        question,
        summary: question && summary[question._id] ? summary[question._id] : [],
        respondents: data ? data.respondents : []
      }
    },
    { setSummary }
  ),
  withState('isAuto', 'setIsAuto', false),
  withState('isVisible', 'setVisible', false),
  withState('index', 'setIndex', null),
  withState('user', 'setUser', null),
  withProps({
    modalTitle: 'Удалить?',
    modalText: 'Вы уверены, что хотите удалить эту строку?'
  }),
  withHandlers({
    handleAutocomplete: ({
      summary,
      question,
      setSummary,
      setIsAuto
    }) => item => {
      if (question) {
        setSummary({
          [question._id]: [...summary, item]
        })
        setIsAuto(false)
      }
    },
    onAddRow: ({ setIsAuto }) => () => setIsAuto(true),
    onDeleteRow: ({
      summary,
      setIndex,
      setVisible,
      setUser
    }) => index => () => {
      setIndex(index)
      setUser(summary[index].name)
      setVisible(true)
    }
  }),

  withStyles(
    theme => ({
      ...CardStyles(theme),
      ...CommonStyles(theme),
      ...CardTableStyles(theme),
      ...styles(theme)
    }),
    {
      theme: true
    }
  ),
  withDialog
)(LinkAnswer)
