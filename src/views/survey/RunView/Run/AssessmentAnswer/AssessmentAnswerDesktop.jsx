import React from 'react'
import classNames from 'classnames'
import Autocomplete from '../../../../../components/Autocomplete'
// @material-ui components
import {
  Table,
  TableBody,
  TableRow,
  TableCell,
  Button
} from '@material-ui/core'
import { Paper } from '@material-ui/core'
import { Typography, TextField, Grid, MenuItem } from '@material-ui/core'
// useful tools
import isEmpty from '../../../../../helpers/is-empty'
// @material-ui/icons
import AddCircle from '@material-ui/icons/AddCircle'
import RemoveCircleOutline from '@material-ui/icons/RemoveCircleOutline'

const AssessmentAnswer = ({
  prevQuestion,
  prevSummary,
  variants,
  summary,
  isAuto,
  onAddRow,
  onDeleteRow,
  onChange,
  respondents,
  handleAutocomplete,
  handlePrevVariants,
  classes
}) => {
  return (
    <React.Fragment>
      {!isEmpty(prevQuestion) &&
        prevQuestion.type === '3' &&
        !isEmpty(prevSummary) && (
          <Grid container className={classes.GridActionButton}>
            <Button
              variant='contained'
              color='secondary'
              onClick={handlePrevVariants}
            >
              Загрузить варианты с пред вопроса
            </Button>
          </Grid>
        )}
      <Table className={classNames(classes.Table, classes.extraTable)}>
        <TableBody>
          {Object.keys(summary).map(key => {
            const { item, variant } = summary[key]
            return (
              <TableRow key={item.code} className={classes.TableBodyRow}>
                <TableCell
                  className={classNames(classes.removeCell, 'icon-remove')}
                >
                  <RemoveCircleOutline
                    className={classes.removeIcon}
                    onClick={onDeleteRow(key)}
                  />
                </TableCell>
                <TableCell className={classes.TableCellTitle}>
                  <Typography variant='h6' className={classes.typoAutoInput}>
                    {item.name}
                  </Typography>
                </TableCell>
                <TableCell width='25%'>
                  {variants && (
                    <TextField
                      select
                      fullWidth
                      id='answerId'
                      value={isEmpty(variant) ? '' : variant}
                      onChange={onChange(key)}
                      margin='none'
                      className={classes.wrapTitle}
                      InputProps={{
                        className: classes.Title
                      }}
                    >
                      {variants.map((item, index) => {
                        return (
                          <MenuItem key={index} value={index}>
                            <Grid
                              container
                              alignItems='baseline'
                              className={classes.gridType}
                              direction='row'
                              wrap='wrap'
                            >
                              <Typography
                                className={classes.typoSelectInputTitle}
                                variant='h6'
                              >
                                {item.title}
                              </Typography>
                            </Grid>
                          </MenuItem>
                        )
                      })}
                    </TextField>
                  )}
                </TableCell>
              </TableRow>
            )
          })}
        </TableBody>
      </Table>

      {isAuto ? (
        <Paper square className={classes.PaperAuto}>
          <Autocomplete
            items={respondents}
            field='name'
            onChange={handleAutocomplete}
            placeholder='ФИО сотрудника'
          />
        </Paper>
      ) : (
        <Grid container className={classes.GridButtonAdd}>
          <AddCircle className={classes.addIcon} onClick={onAddRow} />
        </Grid>
      )}
    </React.Fragment>
  )
}

export default AssessmentAnswer
