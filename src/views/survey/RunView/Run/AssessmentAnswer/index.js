import { compose, withState, withProps, withHandlers } from 'recompose'
import { connect } from 'react-redux'
// AC
import { setSummary } from '../../../../../redux/modules/result'
// Hocs
import withDialog from './withDialog'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import CardStyles from '../../../../../styles/CardStyles'
import CommonStyles from '../../../../../styles/CommonStyles'
import CardTableStyles from '../../../../../styles/CardTableStyles'

import AssessmentAnswer from './AssessmentAnswer'
import isEmpty from '../../../../../helpers/is-empty'

export default compose(
  connect(
    ({ resultStore }) => {
      const { data, stage, question, answers, summary } = resultStore

      const prevQuestion =
        data.questions && stage > 0 ? data.questions[stage - 1] : []
      const prevSummary =
        prevQuestion && !isEmpty(summary) ? summary[prevQuestion._id] : []

      return {
        question,
        prevQuestion,
        prevSummary,
        summary: question && summary[question._id] ? summary[question._id] : [],
        criterias: question.criterias ? question.criterias : [],
        variants: answers.find(a => a._id === '3').variants,
        respondents: data ? data.respondents : []
      }
    },
    { setSummary }
  ),
  withState('isAuto', 'setIsAuto', false),
  withState('isVisible', 'setVisible', false),
  withState('index', 'setIndex', null),
  withState('user', 'setUser', null),
  withProps({
    modalTitle: 'Удалить?',
    modalText: 'Вы уверены, что хотите удалить эту строку?'
  }),
  withHandlers({
    handleAutocomplete: ({
      summary,
      question,
      setSummary,
      setIsAuto
    }) => item => {
      if (question) {
        setSummary({
          [question._id]: { ...summary, [item._id]: { item, variant: '' } }
        })
        setIsAuto(false)
      }
    },
    onAddRow: ({ setIsAuto }) => () => setIsAuto(true),
    onDeleteRow: ({
      summary,
      setIndex,
      setVisible,
      setUser
    }) => index => () => {
      setIndex(index)
      setUser(summary[index].item.name)
      setVisible(true)
    },
    onChange: ({ summary, question, setSummary }) => index => event => {
      const item = summary[index].item
      setSummary({
        [question._id]: {
          ...summary,
          [item._id]: { item, variant: event.target.value }
        }
      })
    },
    handlePrevVariants: ({ prevSummary, question, setSummary }) => () => {
      const newSummary = Object.keys(prevSummary).reduce((acc, key) => {
        return {
          ...acc,
          [key]: {
            item: prevSummary[key].item,
            variant: ''
          }
        }
      }, {})

      setSummary({
        [question._id]: newSummary
      })
    }
  }),

  withStyles(
    theme => ({
      ...CardStyles(theme),
      ...CommonStyles(theme),
      ...CardTableStyles(theme),
      ...styles(theme)
    }),
    {
      theme: true
    }
  ),
  withDialog
)(AssessmentAnswer)
