import React from 'react'
import { Hidden } from '@material-ui/core'
import AssessmentAnswerDesktop from './AssessmentAnswerDesktop'
import AssessmentAnswerMobile from './AssessmentAnswerMobile'

const AssessmentAnswer = props => {
  return (
    <React.Fragment>
      <Hidden xsDown>
        <AssessmentAnswerDesktop {...props} />
      </Hidden>
      <Hidden smUp>
        <AssessmentAnswerMobile {...props} />
      </Hidden>
    </React.Fragment>
  )
}

export default AssessmentAnswer
