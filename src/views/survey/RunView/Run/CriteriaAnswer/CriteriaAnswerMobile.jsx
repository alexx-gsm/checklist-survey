import React from 'react'
import { Typography, TextField, Grid, MenuItem } from '@material-ui/core'
// useful tools
import isEmpty from '../../../../../helpers/is-empty'

const CriteriaAnswerMobile = ({
  criterias,
  variants,
  summary,
  onChange,
  classes
}) => {
  return (
    <Grid
      container
      direction='column'
      wrap='nowrap'
      className={classes.GridContainerMobile}
    >
      {criterias.map((criteria, index) => {
        return (
          <Grid item className={classes.GridItemMobile} key={index}>
            <Typography variant='h6' className={classes.typoAutoInput}>
              {criteria.title}
            </Typography>

            {variants && (
              <TextField
                select
                fullWidth
                id='answerId'
                value={
                  !isEmpty(summary) && !isEmpty(summary[index])
                    ? summary[index]
                    : ''
                }
                onChange={onChange(index)}
                className={classes.wrapTitle}
                InputProps={{
                  className: classes.Title
                }}
              >
                {variants.map(item => {
                  return (
                    <MenuItem key={item._id} value={item._id}>
                      <Grid
                        container
                        alignItems='baseline'
                        className={classes.gridType}
                        direction='row'
                        wrap='wrap'
                      >
                        <Typography
                          className={classes.typoSelectInputTitle}
                          variant='h6'
                        >
                          {item.title}
                        </Typography>
                      </Grid>
                    </MenuItem>
                  )
                })}
              </TextField>
            )}
          </Grid>
        )
      })}
    </Grid>
  )
}

export default CriteriaAnswerMobile
