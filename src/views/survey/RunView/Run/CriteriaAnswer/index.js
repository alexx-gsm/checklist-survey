import { compose, withHandlers } from 'recompose'
import { connect } from 'react-redux'
// AC
import { storeResult, setSummary } from '../../../../../redux/modules/result'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import CardStyles from '../../../../../styles/CardStyles'
import CardTableStyles from '../../../../../styles/CardTableStyles'
// useful tools
import isEmpty from '../../../../../helpers/is-empty'

import CriteriaAnswer from './CriteriaAnswer'

export default compose(
  connect(
    ({ resultStore }) => {
      const { data, summary, question, answers } = resultStore

      return {
        question,
        summary: question && summary[question._id] ? summary[question._id] : [],
        criterias: question.criterias ? question.criterias : [],
        variants: answers.find(a => a._id === '2').variants,
        respondents: data.respondents ? data.respondents : []
      }
    },
    { setSummary }
  ),
  withHandlers({
    onChange: ({ question, summary, setSummary }) => _id => event => {
      setSummary({
        [question._id]: { ...summary, [_id]: event.target.value }
      })
      // storeStageResult({
      //   ...result,
      //   [index]: event.target.value
      // })
    }
  }),
  withStyles(
    theme => ({
      ...CardStyles(theme),
      ...CardTableStyles(theme),
      ...styles(theme)
    }),
    {
      theme: true
    }
  )
)(CriteriaAnswer)
