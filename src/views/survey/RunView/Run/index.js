import React from 'react'
import { withRouter } from 'react-router-dom'
import {
  compose,
  withState,
  withHandlers,
  withProps,
  lifecycle
} from 'recompose'
import { connect } from 'react-redux'
// AC
import {
  getData,
  setStage,
  setPrevResult,
  saveSummary,
  setSummary
} from '../../../../redux/modules/result'
// Styles
import { withStyles } from '@material-ui/core/styles'
import CommonStyles from '../../../../styles/CommonStyles'
import СardStyles from '../../../../styles/CardStyles'
import styles from './styles'
// useful tools
import isEmpty from '../../../../helpers/is-empty'

import Run from './Run'

export default compose(
  connect(
    ({ resultStore }) => {
      const { question, summary, length, stage, link } = resultStore

      return {
        question,
        stage,
        length,
        summary,
        freeAnswer: question.type === '4' ? summary[question._id] : '',
        link
      }
    },
    { getData, setStage, setPrevResult, saveSummary, setSummary }
  ),
  withRouter,
  withState('showSelect', 'setShowSelect', false),
  withHandlers({
    onChange: ({ question, setSummary }) => e => {
      setSummary({
        [question._id]: e.target.value
      })
    },
    handleNext: ({
      stage,
      setStage,
      summary,
      saveSummary,
      length,
      link
    }) => () => {
      if (length > 0 && stage < length - 1) {
        saveSummary(summary, link)
        setStage(stage + 1)
      }
    },
    handleBack: ({
      stage,
      setStage,
      summary,
      saveSummary,
      length,
      link
    }) => () => {
      if (length > 0 && stage > 0) {
        saveSummary(summary, link)
        setStage(stage - 1)
      }
    },
    handleFinish: ({ summary, saveSummary, link, history }) => () => {
      saveSummary(summary, link)

      history.push('/')
    }
  }),
  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params
      if (!isEmpty(id)) {
        this.props.getData(id)
      } else {
        window.location.replace(window.location.origin)
      }
    }
  }),
  withStyles(
    theme => {
      return {
        ...СardStyles(theme),
        ...CommonStyles(theme),
        ...styles(theme)
      }
    },
    { theme: true }
  )
)(Run)
