import { compose, withHandlers } from 'recompose'
import { connect } from 'react-redux'
// AC
import { saveSummary, setStage } from '../../redux/modules/result'
// hocs
import withSidebar from '../../hocs/withSidebar'
import { withStyles } from '@material-ui/core/styles'
// component
import Survey from './Survey'
// styles
import styles from './styles'

export default compose(
  connect(
    ({ resultStore }) => {
      const { data, summary, link } = resultStore
      return {
        questions: data.questions ? data.questions : [],
        summary,
        saveSummary,
        link
      }
    },
    { saveSummary, setStage }
  ),
  withHandlers({
    handleClick: ({ summary, saveSummary, setStage, link }) => index => () => {
      saveSummary(summary, link)
      setStage(index)
    },
    handleFinish: ({ summary, saveSummary, link, history }) => () => {
      saveSummary(summary, link)

      history.push('/')
    }
  }),
  withStyles(styles, { withTheme: true }),
  withSidebar
)(Survey)
