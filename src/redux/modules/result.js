import axios from 'axios'
import { Record } from 'immutable'
import { appName, HOST } from '../../config'
// mocks
import answers from '../mocks/answers'
import isEmpty from '../../helpers/is-empty'

//! --- MODULE NAME ---
export const moduleName = 'resultStore'

//! --- ACTIONS ---
export const RESULT__LOADING = `${appName}/${moduleName}/RESULT__LOADING`
export const RESULT__GET_DATA = `${appName}/${moduleName}/RESULT__GET_DATA`
export const RESULT__SET_STAGE = `${appName}/${moduleName}/RESULT__SET_STAGE`
export const RESULT__SET_SUMMARY = `${appName}/${moduleName}/RESULT__SET_SUMMARY`

export const RESULT__SET_RESULT = `${appName}/${moduleName}/RESULT__SET_RESULT`
export const RESULT__SET_PREV_RESULT = `${appName}/${moduleName}/RESULT__SET_PREV_RESULT`
export const RESULT__SET_RESULTS = `${appName}/${moduleName}/RESULT__SET_RESULTS`
export const RESULT__SET_LINK = `${appName}/${moduleName}/RESULT__SET_LINK`

export const RESULT__REFRESH = `${appName}/${moduleName}/RESULT__REFRESH`
export const RESULT__UPDATE = `${appName}/${moduleName}/RESULT__UPDATE`

export const RESULT__SAVE = `${appName}/${moduleName}/RESULT__SAVE`
export const RESULT__GET_ALL = `${appName}/${moduleName}/RESULT__GET_ALL`
export const RESULT__UPLOAD = `${appName}/${moduleName}/RESULT__UPLOAD`
export const RESULT__ERROR = `${appName}/${moduleName}/RESULT__ERROR`

//! --- INITIAL STATE ---
const ReducerRecord = Record({
  link: '',
  stage: 0,
  length: 0,
  data: {},
  summary: {},
  result: [],
  question: [],
  prevResult: [],
  results: {},
  answers,
  error: {},
  loading: false
})

//! --- REDUCER ---
const reducer = (state = new ReducerRecord(), { type, payload, error }) => {
  const { questions, summary } = state.data
  const question = questions ? questions[state.stage] : []
  // const result = summary[question._id]

  switch (type) {
    case RESULT__LOADING:
      return state.set('loading', true)
    case RESULT__SET_STAGE:
      return state
        .set('loading', false)
        .set('stage', payload)
        .set('question', questions ? questions[payload] : [])
        .set('error', {})

    case RESULT__SET_SUMMARY:
      return state
        .set('loading', false)
        .set('summary', { ...state.summary, ...payload })
        .set('error', {})

    case RESULT__SET_RESULT:
      return state
        .set('loading', false)
        .set('result', payload)
        .set('error', {})
    case RESULT__SET_PREV_RESULT:
      return state
        .set('loading', false)
        .set('prevResult', payload)
        .set('error', {})
    case RESULT__SET_RESULTS:
      return state
        .set('loading', false)
        .set('results', payload)
        .set('error', {})
    case RESULT__GET_DATA:
      const { summary, ...rest } = payload
      return state
        .set('loading', false)
        .set('summary', summary)
        .set('data', rest)
        .set(
          'question',
          payload.questions ? payload.questions[state.stage] : []
        )
        .set('length', payload.questions.length)
        .set('error', {})
    case RESULT__SET_LINK:
      return state
        .set('loading', false)
        .set('link', payload)
        .set('error', {})

    case RESULT__ERROR:
      return state.set('loading', false).set('error', error)
    default:
      return state
  }
}

//! --- AC ---
/************************************************
 * * @desc    SET STAGE
 * ? @param   payload: stage
 ************************************************/
export const setStage = payload => ({
  type: RESULT__SET_STAGE,
  payload
})

/************************************************
 * * @desc    SET SUMMARY
 * ? @param   summary
 ************************************************/
export const setSummary = payload => ({
  type: RESULT__SET_SUMMARY,
  payload
})

/************************************************
 * * @desc    SET RESULT
 * ? @params  payload
 ************************************************/
export const setResult = payload => ({
  type: RESULT__SET_RESULT,
  payload
})

/************************************************
 * * @desc    SET PREV RESULT
 * ? @params  payload
 ************************************************/
export const setPrevResult = payload => ({
  type: RESULT__SET_PREV_RESULT,
  payload
})

/************************************************
 * * @desc    SET RESULTS
 * ? @param   payload: results
 ************************************************/
export const setResults = payload => ({
  type: RESULT__SET_RESULTS,
  payload
})

/************************************************
 * * @desc    SAVE SUMMARY
 * ? @params  payload
 * ? @params  cb: callback function
 ************************************************/
export const saveSummary = (payload, link) => dispatch => {
  axios
    .put(`${HOST}/api/results/data/${link}`, { payload })
    .then(res => {
      // dispatch({
      //   type: RESULT__SET_RESULT,
      //   payload: res.data
      // })
      // if (typeof cb === 'function') cb(res.data)
    })
    .catch(error => {
      dispatch({
        type: RESULT__ERROR,
        error: error.response.data
      })
    })
}

/************************************************
 * * @desc    GET DATA
 * ? @param   _id: result id
 ************************************************/
export const getData = link => dispatch => {
  dispatch({
    type: RESULT__LOADING
  })
  dispatch({
    type: RESULT__SET_LINK,
    payload: link
  })
  axios
    .post(`${HOST}/api/results/data/${link}`)
    .then(res => {
      dispatch({
        type: RESULT__GET_DATA,
        payload: res.data
      })
    })
    .catch(error =>
      dispatch({
        type: RESULT__ERROR,
        error
      })
    )
}

/************************************************
 * * @desc    SET STAGE
 * ? @param   payload: stage
 ************************************************/
export const setLink = payload => ({
  type: RESULT__SET_LINK,
  payload
})

export default reducer
