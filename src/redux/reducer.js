import { combineReducers } from 'redux'
import sidebarReducer, { moduleName as sidebarModule } from './modules/sidebar'
import resultReducer, { moduleName as resultModule } from './modules/result'

export default combineReducers({
  [sidebarModule]: sidebarReducer,
  [resultModule]: resultReducer
})
